# 0xSTRING

**0xSTRING** is a library that tries to be much easier (and hopefully faster) alternative for `C` string.

## How does it work

There are 2 types of strings, the normal one and the tiny one.

### Default String

The structure of data (a.k.a string) is:

```c
typedef char *z0_str;
```

as you can see there is no structure with extra fields that make typing annoying. The idea is taken from [sds](https://github.com/antirez/sds) library.

Basically the size of the string is stored before the actual pointer.

```
+----------+-------------------------+
+   size   +          pointer        +
+----------+-------------------------+
```

Here is an example: Let's say we want to store "LOREM IMPSUM" in a variable, then we can go and use the `z0_string_init` function in order to create a new string.

```c
z0_str str;
str = z0_str_init("LOREM IMPSUM");

//...

z0_str_free(str);
```

- Now the function will return only the pointer to the actual data (which is also **NULL** terminated). The advantage of this approach is that you can use the standard string functions (e.g. `printf`).

```c
z0_str str;
//...

//because [str] points to actual data: we can simple put that in the [printf] function.
printf("%.*s", z0_str_len(str), str);

//...
```

- Another advantage is that you can get the length of the string without iterating over it. Simply use the `z0_string_len` function:

```c
size_t len;
len = z0_str_len(str);
```

and here is the implementation of `z0_str_len`: It just gets the size from the header.

```c
size_t
z0_str_len(z0_str ptr) 
{
        size_t len;
        len = *(size_t*)(ptr - sizeof (size_t));

        return len;
}
```

### Tiny String

This one leverages the x86_64 CPU "properties", which states that only 6 bytes of a pointer will be actually used and the higher order 2 bytes will be zeros. Tiny string uses those two bytes to store the size there. At the end the pointer will look something like this:

```
[S][S][A][A][A][A][A][A]

S - size
A - address
```

This means that unlike the default string the tiny string does NOT allocate an extra space for the size. However there are couple of disatvantages:

- The type is CPU architecture bound. Only `x86_64`s are allowed to have this (I think it would probably work on `ARM` but I don't own one to test it there). 
- The size MUST be an `uint16_t`. Anything bigger than this will not work.
- It's not as nice to use as the default one. Everytime you have to write `tstr_ptr(MY_STRING)` instead of `MY_STRING`.

### Namespace

To remove the headache of writing `z0_*` every time, simply add the `Z0_STR_SHORT_NAMES` symbol before your header file:
```c
#define Z0_STR_SHORT_NAMES
#include "z0_str.h"

// now there is no need for [z0_*]
str text;
text = str_init("abc");
//...
str_free(text);
```


### APIs

- `z0_str_init` copies the given string in a new location in the HEAP and returns it as a `z0_str`:
```c
z0_str
z0_str_init(const char*); 

z0_tstr
z0_tstr_init(const char*); 
```

- `z0_str_init_len` creates a new string in the HEAP with given size. The string containes garbage:
```c
z0_str
z0_str_init_len(size_t);

z0_tstr
z0_tstr_init_len(uint16_t);
```

- `z0_str_init_full` creates a new string in the HEAP with given size and copies all the data from the given `char*`: 
```c
z0_str
z0_str_init_full(const char*, size_t); 

z0_tstr
z0_tstr_init_full(const char*, uint16_t); 
```

- `z0_str_len` returns the length of given string. This operation is constant time:  
```c
size_t
z0_str_len(z0_str);

uint16_t
z0_tstr_len(const z0_tstr);
```

- `z0_tstr_ptr` extracts the last 6 bytes ot of the `z0_tstr` which store the actual address of the string:
```c
char*
z0_tstr_ptr(const z0_tstr);
```

- `z0_str_cmb` concatinates (combines) two strings without damaging them. The returned string is enteriely a new string which needs to be freed sperataly:  
```c
z0_str
z0_str_cmb(z0_str, z0_str);

z0_tstr
z0_tstr_cmb(const z0_tstr, const z0_tstr);
```

- `z0_str_dup` duplicates (clones) the given string: 
```c
z0_str
z0_str_dup(z0_str);

z0_tstr
z0_tstr_dup(const z0_tstr);
```

- `z0_str_cmp` compares the two given strings:
```c
bool
z0_str_cmp(z0_str, z0_str);

bool
z0_tstr_cmp(const z0_tstr, const z0_tstr);
```

- `z0_str_sub` creates a new string based on given `offset` and `length`. The returned string must be freed explicitly: 
```c
z0_str
z0_str_sub(z0_str, size_t, size_t);

z0_tstr
z0_tstr_sub(const z0_tstr, uint16_t, uint16_t);
```

- `z0_str_pos` searches for the substring and returns the first index of it. In case of failure the length of original string is returned:
```c
size_t
z0_str_pos(z0_str, z0_str);

uint16_t
z0_tstr_pos(const z0_tstr, const z0_tstr);
```

- `z0_str_free` frees the HEAP allocated string: 
```c
void
z0_str_free(z0_str);

void
z0_tstr_free(z0_tstr);
```

- `z0_str_print` prints the string on `STDOUT`:
```c
void
z0_str_print(z0_str); 

void
z0_tstr_print(z0_tstr);
```

### Testing

To run unit tests, simply type `make` or `make test` and something like this should appear on the screen:
```
========== TEST DEFAULT INITIALIZATION ==========
RESULT: PASSED
RESULT: PASSED
========== TEST INITIALIZATION WITH PREDEFINED LENGTH ==========
RESULT: PASSED
========== TEST INITIALIZATION WITH LENGTH AND VALUE ==========
RESULT: PASSED
RESULT: PASSED
========== TEST GETTING THE LENGTH ==========
RESULT: PASSED
========== TEST CONCATINATION ==========
RESULT: PASSED
RESULT: PASSED
========== TEST COPY ==========
RESULT: PASSED
RESULT: PASSED
========== TEST COMPARSION ==========
RESULT: PASSED
RESULT: PASSED
RESULT: PASSED
RESULT: PASSED
========== TEST SUBSTRING ==========
RESULT: PASSED
RESULT: PASSED
========== TEST GET POSITION OF SUBSTRING ==========
RESULT: PASSED
RESULT: PASSED
RESULT: PASSED
RESULT: PASSED
```
