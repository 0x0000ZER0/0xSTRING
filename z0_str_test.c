#include "z0_test.h"

#define Z0_STR_SHORT_NAMES
#include "z0_str.h"

#include <string.h>

void
test_z0_str_init(void)
{
	Z0_TEST_NAME("TEST DEFAULT INITIALIZATION");

	const char orig[] = "lorem ipsum";

	str text;
	text = str_init(orig);

	size_t text_size;
	text_size = str_len(text);

	Z0_TEST_ASSERT(text_size == sizeof (orig) - 1);
	Z0_TEST_ASSERT(strncmp(text, orig, text_size) == 0);

	str_free(text);
}

void
test_z0_str_init_len(void)
{
	Z0_TEST_NAME("TEST INITIALIZATION WITH PREDEFINED LENGTH");

	const size_t size = 64;

	str text;
	text = str_init_len(size);

	size_t text_size;
	text_size = str_len(text);

	Z0_TEST_ASSERT(text_size == size);

	str_free(text);
}

void
test_z0_str_init_full(void)
{
	Z0_TEST_NAME("TEST INITIALIZATION WITH LENGTH AND VALUE");

	const char   orig[]    = "lorem ipsum";
	const size_t orig_size = sizeof (orig) - 1;

	str text;
	text = str_init_full(orig, orig_size);

	size_t text_size;
	text_size = str_len(text);

	Z0_TEST_ASSERT(text_size == orig_size);
	Z0_TEST_ASSERT(strncmp(text, orig, orig_size) == 0);

	str_free(text);
}

void
test_z0_str_len(void)
{
	Z0_TEST_NAME("TEST GETTING THE LENGTH");
	
	str text;
	text = str_init("0123");

	size_t size;
	size = str_len(text);

	Z0_TEST_ASSERT(size == sizeof ("0123") - 1);

	str_free(text);
}

void
test_z0_str_cmb(void)
{
	Z0_TEST_NAME("TEST CONCATINATION");

	str text1;
	text1 = str_init("012");

	size_t size1;
	size1 = str_len(text1);
	
	str text2;
	text2 = str_init("3456");
	
	size_t size2;
	size2 = str_len(text2);

	str text_cmb;
	text_cmb = str_cmb(text1, text2);

	size_t text_cmb_len;
	text_cmb_len = str_len(text_cmb);

	Z0_TEST_ASSERT(text_cmb_len == size1 + size2);
	Z0_TEST_ASSERT(strncmp("0123456", text_cmb, text_cmb_len) == 0);

	str_free(text_cmb);
	str_free(text2);
	str_free(text1);
}

void
test_z0_str_dup(void)
{
	Z0_TEST_NAME("TEST COPY");

	str orig;
	orig = str_init("abc");

	str copy;
	copy = str_dup(orig);

	Z0_TEST_ASSERT(str_len(orig) == str_len(copy));
	Z0_TEST_ASSERT(strncmp(orig, copy, str_len(orig)) == 0);

	str_free(orig);
	str_free(copy);	
}

void
test_z0_str_cmp(void)
{
	Z0_TEST_NAME("TEST COMPARSION");

	str text1;
	text1 = str_init("abc");

	str text2;
	text2 = str_init("abc");

	str text3;
	text3 = str_init("abcd");

	Z0_TEST_ASSERT(str_len(text1) == str_len(text2));
	Z0_TEST_ASSERT(str_cmp(text1, text2));
	Z0_TEST_ASSERT(str_len(text1) != str_len(text3));
	Z0_TEST_ASSERT(!str_cmp(text1, text3));

	str_free(text3);
	str_free(text2);
	str_free(text1);	
}

void
test_z0_str_sub(void)
{
	Z0_TEST_NAME("TEST SUBSTRING");
	
	str orig;
	orig = str_init("0123456789");

	const size_t off = 3;
	const size_t len = 4;

	str sub;
	sub = str_sub(orig, off, len);

	Z0_TEST_ASSERT(str_len(sub) == len);
	Z0_TEST_ASSERT(strncmp(sub, "3456", len) == 0);

	str_free(sub);
	str_free(orig);
}

void
test_z0_str_pos(void)
{
	Z0_TEST_NAME("TEST GET POSITION OF SUBSTRING");

	str orig;
	orig = str_init("0123456789");

	str sub1;
	sub1 = str_init("2345");

	size_t pos1;
	pos1 = str_pos(orig, sub1);

	str sub2;
	sub2 = str_init("78");

	size_t pos2;
	pos2 = str_pos(orig, sub2);

	str sub3;
	sub3 = str_init("321");

	size_t pos3;
	pos3 = str_pos(orig, sub3);

	str sub4;
	sub4 = str_init("890");

	size_t pos4;
	pos4 = str_pos(orig, sub3);

	Z0_TEST_ASSERT(pos1 == 2);
	Z0_TEST_ASSERT(pos2 == 7);
	Z0_TEST_ASSERT(pos3 == str_len(orig));
	Z0_TEST_ASSERT(pos4 == str_len(orig));

	str_free(sub4);
	str_free(sub3);
	str_free(sub2);
	str_free(sub1);
	str_free(orig);
}

#if __x86_64__

void
test_z0_tstr_init(void)
{
	Z0_TEST_NAME("TEST DEFAULT INITIALIZATION (TINY STRING)");

	const char orig[] = "lorem ipsum";

	tstr text;
	text = tstr_init(orig);

	uint16_t text_size;
	text_size = tstr_len(text);

	Z0_TEST_ASSERT(text_size == sizeof (orig) - 1);
	Z0_TEST_ASSERT(strncmp(tstr_ptr(text), orig, text_size) == 0);

	tstr_free(text);
}

void
test_z0_tstr_init_len(void)
{
	Z0_TEST_NAME("TEST INITIALIZATION WITH PREDEFINED LENGTH (TINY STRING)");

	const uint16_t size = 64;

	tstr text;
	text = tstr_init_len(size);

	uint16_t text_size;
	text_size = tstr_len(text);

	Z0_TEST_ASSERT(text_size == size);

	tstr_free(text);
}

void
test_z0_tstr_init_full(void)
{
	Z0_TEST_NAME("TEST INITIALIZATION WITH LENGTH AND VALUE (TINY STRING)");

	const char	orig[]    = "lorem ipsum";
	const uint16_t	orig_size = sizeof (orig) - 1;

	tstr text;
	text = tstr_init_full(orig, orig_size);

	uint16_t text_size;
	text_size = tstr_len(text);

	Z0_TEST_ASSERT(text_size == orig_size);
	Z0_TEST_ASSERT(strncmp(tstr_ptr(text), orig, orig_size) == 0);

	tstr_free(text);
}

void
test_z0_tstr_len(void)
{
	Z0_TEST_NAME("TEST GETTING THE LENGTH (TINY STRING)");
	
	tstr text;
	text = tstr_init("0123");

	uint16_t size;
	size = tstr_len(text);

	Z0_TEST_ASSERT(size == sizeof ("0123") - 1);

	tstr_free(text);
}

void
test_z0_tstr_ptr(void)
{
	Z0_TEST_NAME("TEST GETTING THE POINTER OF A TINY STRING");
	
	tstr text;
	text = tstr_init("0123");

	uint16_t size;
	size = tstr_len(text);

	char *ptr;
	ptr = tstr_ptr(text);

	Z0_TEST_ASSERT(size == sizeof ("0123") - 1);
	Z0_TEST_ASSERT(strncmp(ptr, "0123", size) == 0);

	tstr_free(text);
}

void
test_z0_tstr_cmb(void)
{
	Z0_TEST_NAME("TEST CONCATINATION (TINY STRING)");

	tstr text1;
	text1 = tstr_init("012");

	uint16_t size1;
	size1 = tstr_len(text1);
	
	tstr text2;
	text2 = tstr_init("3456");
	
	uint16_t size2;
	size2 = tstr_len(text2);

	tstr text_cmb;
	text_cmb = tstr_cmb(text1, text2);

	uint16_t text_cmb_len;
	text_cmb_len = tstr_len(text_cmb);

	Z0_TEST_ASSERT(text_cmb_len == size1 + size2);
	Z0_TEST_ASSERT(strncmp("0123456", tstr_ptr(text_cmb), text_cmb_len) == 0);

	tstr_free(text_cmb);
	tstr_free(text2);
	tstr_free(text1);
}

void
test_z0_tstr_dup(void)
{
	Z0_TEST_NAME("TEST COPY (TINY STRING)");

	tstr orig;
	orig = tstr_init("abc");

	tstr copy;
	copy = tstr_dup(orig);

	Z0_TEST_ASSERT(tstr_len(orig) == tstr_len(copy));
	Z0_TEST_ASSERT(strncmp(tstr_ptr(orig), tstr_ptr(copy), tstr_len(orig)) == 0);

	tstr_free(orig);
	tstr_free(copy);	
}

void
test_z0_tstr_cmp(void)
{
	Z0_TEST_NAME("TEST COMPARSION (TINY STRING)");

	tstr text1;
	text1 = tstr_init("abc");

	tstr text2;
	text2 = tstr_init("abc");

	tstr text3;
	text3 = tstr_init("abcd");

	Z0_TEST_ASSERT(tstr_len(text1) == tstr_len(text2));
	Z0_TEST_ASSERT(tstr_cmp(text1, text2));
	Z0_TEST_ASSERT(tstr_len(text1) != tstr_len(text3));
	Z0_TEST_ASSERT(!tstr_cmp(text1, text3));

	tstr_free(text3);
	tstr_free(text2);
	tstr_free(text1);	
}

void
test_z0_tstr_sub(void)
{
	Z0_TEST_NAME("TEST SUBSTRING (TINY STRING)");
	
	tstr orig;
	orig = tstr_init("0123456789");

	const uint16_t off = 3;
	const uint16_t len = 4;

	tstr sub;
	sub = tstr_sub(orig, off, len);

	Z0_TEST_ASSERT(tstr_len(sub) == len);
	Z0_TEST_ASSERT(strncmp(tstr_ptr(sub), "3456", len) == 0);

	tstr_free(sub);
	tstr_free(orig);
}

void
test_z0_tstr_pos(void)
{
	Z0_TEST_NAME("TEST GET POSITION OF SUBSTRING (TINY STRING)");

	tstr orig;
	orig = tstr_init("0123456789");

	tstr sub1;
	sub1 = tstr_init("2345");

	uint16_t pos1;
	pos1 = tstr_pos(orig, sub1);

	tstr sub2;
	sub2 = tstr_init("78");

	uint16_t pos2;
	pos2 = tstr_pos(orig, sub2);

	tstr sub3;
	sub3 = tstr_init("321");

	uint16_t pos3;
	pos3 = tstr_pos(orig, sub3);

	tstr sub4;
	sub4 = tstr_init("890");

	uint16_t pos4;
	pos4 = tstr_pos(orig, sub3);

	Z0_TEST_ASSERT(pos1 == 2);
	Z0_TEST_ASSERT(pos2 == 7);
	Z0_TEST_ASSERT(pos3 == tstr_len(orig));
	Z0_TEST_ASSERT(pos4 == tstr_len(orig));

	tstr_free(sub4);
	tstr_free(sub3);
	tstr_free(sub2);
	tstr_free(sub1);
	tstr_free(orig);
}

#endif

int
main(void)
{
	test_z0_str_init();
	test_z0_str_init_len();
	test_z0_str_init_full();
	test_z0_str_len();
	test_z0_str_cmb();
	test_z0_str_dup();
	test_z0_str_cmp();
	test_z0_str_sub();
	test_z0_str_pos();

#if __x86_64__

	test_z0_tstr_init();
	test_z0_tstr_init_len();
	test_z0_tstr_init_full();
	test_z0_tstr_len();
	test_z0_tstr_ptr();
	test_z0_tstr_cmb();
	test_z0_tstr_dup();
	test_z0_tstr_cmp();
	test_z0_tstr_sub();
	test_z0_tstr_pos();

#endif

	return 0;
}
