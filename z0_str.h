#ifndef Z0_STR_H
#define Z0_STR_H

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

typedef char *z0_str;

z0_str
z0_str_init(const char*); 

z0_str
z0_str_init_len(size_t); 

z0_str
z0_str_init_full(const char*, size_t); 

size_t
z0_str_len(z0_str);

z0_str
z0_str_cmb(z0_str, z0_str);

z0_str
z0_str_dup(z0_str);

bool
z0_str_cmp(z0_str, z0_str);

z0_str
z0_str_sub(z0_str, size_t, size_t);

size_t
z0_str_pos(z0_str, z0_str);

void
z0_str_free(z0_str);

void
z0_str_print(z0_str);

#if __x86_64__

typedef char *z0_tstr;

z0_tstr
z0_tstr_init(const char*);

z0_tstr
z0_tstr_init_len(uint16_t); 

z0_tstr
z0_tstr_init_full(const char*, uint16_t); 

uint16_t
z0_tstr_len(const z0_tstr);

char*
z0_tstr_ptr(const z0_tstr);

z0_tstr
z0_tstr_cmb(const z0_tstr, const z0_tstr);

z0_tstr
z0_tstr_dup(const z0_tstr);

bool
z0_tstr_cmp(const z0_tstr, const z0_tstr);

z0_tstr
z0_tstr_sub(const z0_tstr, uint16_t, uint16_t);

uint16_t
z0_tstr_pos(const z0_tstr, const z0_tstr);

void
z0_tstr_free(z0_str);

void
z0_tstr_print(const z0_tstr);

#endif

#ifdef Z0_STR_SHORT_NAMES
#define str	 	z0_str
#define str_init	z0_str_init
#define str_init_len	z0_str_init_len
#define str_init_full	z0_str_init_full
#define str_len		z0_str_len
#define str_cmb		z0_str_cmb
#define str_dup		z0_str_dup
#define str_cmp		z0_str_cmp
#define str_sub		z0_str_sub
#define str_pos		z0_str_pos
#define str_free	z0_str_free
#define str_print	z0_str_print

#if __x86_64__

#define tstr	 	z0_tstr
#define tstr_init	z0_tstr_init
#define tstr_init_len	z0_tstr_init_len
#define tstr_init_full	z0_tstr_init_full
#define tstr_len	z0_tstr_len
#define tstr_ptr	z0_tstr_ptr
#define tstr_cmb	z0_tstr_cmb
#define tstr_dup	z0_tstr_dup
#define tstr_cmp	z0_tstr_cmp
#define tstr_sub	z0_tstr_sub
#define tstr_pos	z0_tstr_pos
#define tstr_free	z0_tstr_free
#define tstr_print	z0_tstr_print

#endif

#endif

#endif
