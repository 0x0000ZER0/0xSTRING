#include "z0_str.h"

#include <malloc.h>
#include <stdio.h>
#include <string.h>

z0_str
z0_str_init(const char *str) 
{
        size_t len;
        len = strlen(str);

        z0_str ptr;
	ptr = malloc(sizeof (size_t) + len * sizeof (char));

        *(size_t*)ptr = len; 

        ptr += sizeof (size_t);
	memcpy(ptr, str, len);	

        return ptr;
}

z0_str
z0_str_init_len(size_t len) 
{
        z0_str ptr;

        ptr = malloc(sizeof (size_t) + len * sizeof (char));

        *(size_t*)ptr = len; 
	ptr += sizeof (size_t);

        return ptr;
}

z0_str
z0_str_init_full(const char *str, size_t len) 
{
        z0_str ptr;
        ptr = malloc(sizeof (size_t) + (len + 1) * sizeof(char));

        *(size_t*)ptr = len;

        ptr += sizeof (size_t);  

	memcpy(ptr, str, len);
	ptr[len] = '\0';

        return ptr;
}

size_t
z0_str_len(z0_str ptr) 
{
        size_t len;

        len = *(size_t*)(ptr - sizeof (size_t));

        return len;
}

z0_str
z0_str_cmb(z0_str str1, z0_str str2) 
{
        size_t len1;
        size_t len2;

        len1 = *(size_t*)(str1 - sizeof (size_t));
        len2 = *(size_t*)(str2 - sizeof (size_t));

        size_t len;
        len = len1 + len2;

        z0_str ptr;
        ptr = malloc(sizeof (size_t) + len * sizeof (char));

        *(size_t*)ptr = len;

        ptr += sizeof (size_t);

	memcpy(ptr, str1, len1);
	memcpy(ptr + len1, str2, len2);

        return ptr;
}

z0_str
z0_str_dup(z0_str ptr) 
{
        size_t len;
        len = *(size_t*)(ptr - sizeof (size_t));

        z0_str str;
        str = malloc(sizeof (size_t) + len * sizeof (char));

        *(size_t*)str = len;

        str += sizeof (size_t);
	memcpy(str, ptr, len);
        
        return str;
}

bool
z0_str_cmp(z0_str str1, z0_str str2) 
{
        size_t len1;
        size_t len2;

        len1 = *(size_t*)(str1 - sizeof (size_t));
        len2 = *(size_t*)(str2 - sizeof (size_t));

        if (len1 == len2)
                return memcmp(str1, str2, len1) == 0;
	else
		return false;
}

z0_str
z0_str_sub(z0_str str, size_t off, size_t len)
{
	z0_str ptr;
	ptr = malloc(sizeof (size_t) + len * sizeof (char));

	*(size_t*)ptr = len;
	
	ptr += sizeof (size_t);
	
	memcpy(ptr, str + off, len);
	return ptr;
}

size_t
z0_str_pos(z0_str orig, z0_str sub)
{
	size_t orig_len;
	orig_len = *(size_t*)(orig - sizeof (size_t));

	size_t sub_len;
	sub_len = *(size_t*)(sub - sizeof (size_t));

	if (sub_len > orig_len)
		return orig_len;

	size_t j;
	for (size_t i = 0; i < orig_len; ++i) {	
		for (j = 0; j < sub_len && i < orig_len; ++j) {
			if (orig[i] != sub[j])
				break;
			++i;
		}

		if (j == sub_len)
			return i - sub_len;
	}

	return orig_len;
}

void
z0_str_free(z0_str ptr) 
{
        ptr -= sizeof (size_t);

        free(ptr);
}

void
z0_str_print(z0_str str) 
{
        size_t len;
        len = z0_str_len(str);

        printf("%.*s", (int)len, str);
}

#if __x86_64__

z0_tstr
z0_tstr_init(const char *str)
{
	uint16_t len;
	len = strlen(str);

	z0_tstr ptr;
	ptr = malloc(len * sizeof (char));

	memcpy(ptr, str, len);
	ptr = (z0_tstr)((ptrdiff_t)ptr | ((ptrdiff_t)len << 6 * 8));

	return ptr;
}

z0_tstr
z0_tstr_init_len(uint16_t len)
{
	z0_tstr ptr;
	ptr = malloc(len * sizeof (char));

	ptr = (z0_tstr)((ptrdiff_t)ptr | ((ptrdiff_t)len << 6 * 8));

	return ptr;
}

z0_tstr
z0_tstr_init_full(const char *str, uint16_t len)
{
	z0_tstr ptr;
	ptr = malloc(len * sizeof (char));

	memcpy(ptr, str, len);
	ptr = (z0_tstr)((ptrdiff_t)ptr | ((ptrdiff_t)len << 6 * 8));

	return ptr;
}

uint16_t
z0_tstr_len(const z0_tstr str)
{
	uint16_t len;
	len = (uint16_t)(((ptrdiff_t)str & (0xFFFFULL << 6 * 8)) >> 6 * 8);

	return len;
}

char*
z0_tstr_ptr(const z0_tstr str)
{
	char *ptr;
	ptr = (char*)((ptrdiff_t)str & 0x0000FFFFFFFFFFFF);

	return ptr;
}

z0_tstr
z0_tstr_cmb(const z0_tstr str1, const z0_tstr str2)
{
	uint16_t len1;
	len1 = z0_tstr_len(str1);

	uint16_t len2;
	len2 = z0_tstr_len(str2);

	uint16_t len;
	len = len1 + len2;

	z0_tstr ptr;
	ptr = malloc(len * sizeof (char));

	memcpy(ptr, z0_tstr_ptr(str1), len1);
	memcpy(ptr + len1, z0_tstr_ptr(str2), len2);

	ptr = (z0_tstr)((ptrdiff_t)ptr | ((ptrdiff_t)len << 6 * 8));

	return ptr;
}

z0_tstr
z0_tstr_dup(const z0_tstr str)
{
        uint16_t len;
        len = z0_tstr_len(str);

        z0_tstr ptr;
        ptr = malloc(len * sizeof (char));

	memcpy(ptr, z0_tstr_ptr(str), len);
	ptr = (z0_tstr)((ptrdiff_t)ptr | ((ptrdiff_t)len << 6 * 8));
        
        return ptr;
}

bool
z0_tstr_cmp(z0_tstr str1, z0_tstr str2) 
{
        uint16_t len1;
        uint16_t len2;

        len1 = z0_tstr_len(str1);
        len2 = z0_tstr_len(str2);

        if (len1 == len2)
                return memcmp(z0_tstr_ptr(str1), z0_tstr_ptr(str2), len1) == 0;
	else
		return false;
}

z0_tstr
z0_tstr_sub(const z0_tstr str, uint16_t off, uint16_t len)
{
	z0_tstr ptr;
	ptr = malloc(len * sizeof (char));

	memcpy(ptr, z0_tstr_ptr(str) + off, len);
	ptr = (z0_tstr)((ptrdiff_t)ptr | ((ptrdiff_t)len << 6 * 8));

	return ptr;
}

uint16_t
z0_tstr_pos(const z0_tstr orig_str, const z0_tstr sub_str)
{
	uint16_t orig_len;
	orig_len = z0_tstr_len(orig_str);

	size_t sub_len;
	sub_len = z0_tstr_len(sub_str);

	if (sub_len > orig_len)
		return orig_len;

	char *orig;
	orig = z0_tstr_ptr(orig_str);

	char *sub;
	sub = z0_tstr_ptr(sub_str);

	size_t j;
	for (size_t i = 0; i < orig_len; ++i) {	
		for (j = 0; j < sub_len && i < orig_len; ++j) {
			if (orig[i] != sub[j])
				break;
			++i;
		}

		if (j == sub_len)
			return i - sub_len;
	}

	return orig_len;
}

void
z0_tstr_free(z0_tstr str)
{
	char *ptr;
	ptr = z0_tstr_ptr(str);

	free(ptr);
}

void
z0_tstr_print(const z0_tstr str)
{
	printf("%.*s", z0_tstr_len(str), z0_tstr_ptr(str));
}

#endif
