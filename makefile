SRC=z0_str.c
OUT=str.out

.PHONY:test
test:
	gcc -g -I0xTEST -Wall -Wextra -fsanitize=leak,address,undefined $(SRC) z0_str_test.c -o $(OUT) \
	&& ./$(OUT) \
	&& rm ./$(OUT)

.PHONY:shared
shared:
	gcc -O2 $(SRC) -shared -o z0_str.so

